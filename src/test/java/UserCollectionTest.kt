import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import database.memorymodel.MemoryUserCollection
import database.mongomodel.MongoUserCollection
import route.user.model.UserCollection
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class UserCollectionTest {
    private var collections = listOf<UserCollection>()

    @BeforeEach
    fun reset() {
        resetTestMongo()
        collections = listOf(MemoryUserCollection(), MongoUserCollection(getTestMongoCollection("users")))
    }

    @TestFactory
    fun createUser(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val user = userCollection.createUser("test_name", "1234", "google")
            assertEquals("test_name", user.name)
        } }
    }

    @TestFactory
    fun createDuplicateUser(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            userCollection.createUser("name", "1234", "google")
            val e = assertThrows(Exception::class.java) { userCollection.createUser("name", "1234", "google") }
            assertEquals("User already exists with that oAuth ID and provider", e.message)
        } }
    }

    @TestFactory
    fun getExistingUserById(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val userOne = userCollection.createUser("name", "1234", "google")
            val userTwo = userCollection.getUser(userOne.id)
            assert(usersAreEqual(userOne, userTwo))
        } }
    }

    @TestFactory
    fun getNonExistingUserById(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val id = "fake_user_id"
            val e = assertThrows(Exception::class.java) { userCollection.getUser(id) }
            assertEquals("User does not exist with id: $id", e.message)
        } }
    }

    @TestFactory
    fun getExistingUserByOAuth(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val userOne = userCollection.createUser("name", "1234", "google")
            val userTwo = userCollection.getUser("1234", "google")
            assert(usersAreEqual(userOne, userTwo))
        } }
    }

    @TestFactory
    fun getNonExistingUserByOAuth(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val oAuthId = "fake_oauth_id"
            val oAuthProvider = "fake_oauth_provider"
            val e = assertThrows(Exception::class.java) { userCollection.getUser(oAuthId, oAuthProvider) }
            assertEquals("User does not exist with oAuthId of $oAuthId and oAuthProvider of $oAuthProvider", e.message)
        } }
    }

    @TestFactory
    fun setUserNameSucceeds(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val userOne = userCollection.createUser("name", "1234", "google")
            userCollection.setUserName(userOne.id, "different_name")
            val userTwo = userCollection.getUser(userOne.id)
            assertEquals("different_name", userTwo.name)
        } }
    }

    @TestFactory
    fun setUserNameFailsWithFakeUserId(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val e = assertThrows(Exception::class.java) { userCollection.setUserName("fake_user_id", "name") }
            assertEquals("User does not exist with id: fake_user_id", e.message)
        } }
    }

    @TestFactory
    fun setUserNameFailsWithEmptyName(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val userOne = userCollection.createUser("name", "1234", "google")
            val e = assertThrows(Exception::class.java) { userCollection.setUserName(userOne.id, "") }
            assertEquals("Cannot set name to an empty string", e.message)
        } }
    }

    @TestFactory
    fun getUsersByIdList(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val userIds = ArrayList<String>()
            for (i in 1..100) {
                val user = userCollection.createUser("name$i", i.toString(), "google")
                userIds.add(user.id)
            }
            val users = userCollection.getUsers(userIds)
            users.forEachIndexed { i, userModel ->
                assertEquals("name${i + 1}", userModel.name)
            }
        } }
    }

    @TestFactory
    fun iterateOverUsers(): List<DynamicTest> {
        return collections.map { userCollection -> DynamicTest.dynamicTest(userCollection::class.java.toString()) {
            val userIdSet = HashSet<String>()
            for (i in 1..1000) {
                val user = userCollection.createUser("name$i", i.toString(), "google")
                userIdSet.add(user.id)
            }
            val userIds = HashSet<String>()
            userCollection.forEachUser { userIds.add(it.id) }

            assertEquals(1000, userIds.size)
            userIds.forEach { assertTrue(userIdSet.contains(it)) }
        } }
    }
}