package route.card

import database.DatabaseCollection
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import route.JsonPatchItem
import route.card.model.BlackCardModel
import route.card.model.CardpackModel
import route.card.model.WhiteCardModel
import route.user.model.UserModel

@RestController
class CardController(private val database: DatabaseCollection) {
    @RequestMapping(value = "/{userId}/cardpack", method = [RequestMethod.PUT])
    @ApiOperation(value = "Create a cardpack")
    @ApiResponses(
            ApiResponse(code = 200, message = "Cardpack created"),
            ApiResponse(code = 400, message = "Invalid request body"),
            ApiResponse(code = 404, message = "User does not exist")
    )
    fun createCardpack(@PathVariable userId: String, @RequestBody doc: JsonCardpack): ResponseEntity<CardpackModel> {
        var user: UserModel

        try {
            user = database.getUser(userId)
        } catch (e: Exception) {
            return ResponseEntity.notFound().build()
        }

        return try {
            val cardpack = database.createCardpack(doc.name, user.id)
            ResponseEntity.ok(cardpack)
        } catch (e: Exception) {
            ResponseEntity.badRequest().build()
        }
    }

    @RequestMapping(value = "/cardpack/{id}", method = [RequestMethod.GET])
    @ApiOperation(value = "Get a cardpack")
    @ApiResponses(
            ApiResponse(code = 200, message = "Cardpack retrieved"),
            ApiResponse(code = 404, message = "Cardpack does not exist")
    )
    fun getCardpack(@PathVariable id: String): ResponseEntity<CardpackModel> {
        return try {
            val cardpack = database.getCardpack(id)
            ResponseEntity.ok(cardpack)
        } catch (e: Exception) {
            ResponseEntity.notFound().build()
        }
    }

    @RequestMapping(value = "/{userId}/cardpacks", method = [RequestMethod.GET])
    @ApiOperation(value = "Get all cardpacks belonging to a certain user")
    @ApiResponses(
            ApiResponse(code = 200, message = "Cardpacks retrieved"),
            ApiResponse(code = 404, message = "User does not exist")
    )
    fun getCardpacksByUser(@PathVariable userId: String): ResponseEntity<List<CardpackModel>> {
        return try {
            val cardpacks = database.getCardpacks(userId)
            ResponseEntity.ok(cardpacks)
        } catch (e: Exception) {
            ResponseEntity.notFound().build()
        }
    }

    @RequestMapping(value = "/cardpack/{id}", method = [RequestMethod.PATCH])
    @ApiOperation(value = "Edit a cardpack")
    @ApiResponses(
            ApiResponse(code = 200, message = "Patch succeeded"),
            ApiResponse(code = 400, message = "Invalid request body"),
            ApiResponse(code = 404, message = "Cardpack does not exist")
    )
    fun patchCardpack(@RequestBody patchDoc: List<JsonPatchItem>, @PathVariable id: String): ResponseEntity<Void> {
        val cardpack: CardpackModel

        try {
            cardpack = database.getCardpack(id)
        } catch (e: Exception) {
            return ResponseEntity.notFound().build()
        }

        patchDoc.forEach { doc ->
            run {
                when {
                    doc.op == "replace" && doc.path == "/name" -> cardpack.setName(doc.value as String)
                    else -> return ResponseEntity.badRequest().build()
                }
            }
        }
        return ResponseEntity.ok().build()
    }

    @RequestMapping(value = "/cardpack/{id}", method = [RequestMethod.DELETE])
    @ApiOperation(value = "Delete a cardpack")
    @ApiResponses(
            ApiResponse(code = 200, message = "Cardpack successfully deleted"),
            ApiResponse(code = 404, message = "Cardpack does not exist")
    )
    fun deleteCardpack(@PathVariable id: String): ResponseEntity<Void> {
        return try {
            database.deleteCardpack(id)
            ResponseEntity.ok().build()
        } catch (e: Exception) {
            ResponseEntity.notFound().build()
        }
    }

    @RequestMapping(value = "/cardpack/{id}/cards/white", method = [RequestMethod.PUT])
    @ApiOperation(value = "Create white cards")
    @ApiResponses(
            ApiResponse(code = 200, message = "Cards successfully created"),
            ApiResponse(code = 400, message = "Invalid request body"),
            ApiResponse(code = 404, message = "Cardpack does not exist")
    )
    fun createWhiteCards(@RequestBody cards: List<JsonWhiteCard>, @PathVariable id: String): ResponseEntity<List<WhiteCardModel>> {
        try {
            database.getCardpack(id)
        } catch (e: Exception) {
            return ResponseEntity.notFound().build()
        }
        val cards = database.createWhiteCards(cards, id)
        return ResponseEntity.ok(cards)
    }

    @RequestMapping(value = "/cardpack/{id}/cards/black", method = [RequestMethod.PUT])
    @ApiOperation(value = "Create black cards")
    @ApiResponses(
            ApiResponse(code = 200, message = "Cards successfully created"),
            ApiResponse(code = 400, message = "Invalid request body"),
            ApiResponse(code = 404, message = "Cardpack does not exist")
    )
    fun createBlackCards(@RequestBody cards: List<JsonBlackCard>, @PathVariable id: String): ResponseEntity<List<BlackCardModel>> {
        try {
            database.getCardpack(id)
        } catch (e: Exception) {
            return ResponseEntity.notFound().build()
        }
        val cards = database.createBlackCards(cards, id)
        return ResponseEntity.ok(cards)
    }

    @RequestMapping(value = "/cardpack/{cardpackId}/cards/white/{cardId}", method = [RequestMethod.DELETE])
    @ApiOperation(value = "Delete white card")
    @ApiResponses(
            ApiResponse(code = 200, message = "Cards successfully created"),
            ApiResponse(code = 404, message = "Card or cardpack does not exist")
    )
    fun deleteWhiteCard(@PathVariable cardpackId: String, @PathVariable cardId: String): ResponseEntity<Void> {
        return try {
            database.deleteWhiteCard(cardId, cardpackId)
            ResponseEntity.ok().build()
        } catch (e: Exception) {
            ResponseEntity.notFound().build()
        }
    }

    @RequestMapping(value = "/cardpack/{cardpackId}/cards/black/{cardId}", method = [RequestMethod.DELETE])
    @ApiOperation(value = "Delete black card")
    @ApiResponses(
            ApiResponse(code = 200, message = "Cards successfully created"),
            ApiResponse(code = 404, message = "Card or cardpack does not exist")
    )
    fun deleteBlackCard(@PathVariable cardpackId: String, @PathVariable cardId: String): ResponseEntity<Void> {
        return try {
            database.deleteBlackCard(cardId, cardpackId)
            ResponseEntity.ok().build()
        } catch (e: Exception) {
            ResponseEntity.notFound().build()
        }
    }

    @RequestMapping(value = "/user/{userId}/cardpacks/favorite", method = [RequestMethod.PUT])
    @ApiOperation(value = "Favorite a cardpack")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully favorited cardpack"),
            ApiResponse(code = 400, message = "Cardpack is already in favorites or is owned by you"),
            ApiResponse(code = 404, message = "Cardpack/user does not exist")
    )
    fun favoriteCardpack(@PathVariable userId: String, @RequestParam(value = "cardpackId", required = true) cardpackId: String): ResponseEntity<Void> {
        try {
            database.getUser(userId)
            database.getCardpack(cardpackId)
        } catch (e: Exception) {
            return ResponseEntity.notFound().build()
        }

        return try {
            database.favoriteCardpack(userId, cardpackId)
            ResponseEntity.ok().build()
        } catch (e: Exception) {
            ResponseEntity.badRequest().build()
        }
    }

    @RequestMapping(value = "/user/{userId}/cardpacks/favorite", method = [RequestMethod.DELETE])
    @ApiOperation(value = "Unfavorite a cardpack")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully unfavorited cardpack"),
            ApiResponse(code = 400, message = "Cardpack is already unfavorited"),
            ApiResponse(code = 404, message = "Cardpack/user does not exist")
    )
    fun unfavoriteCardpack(@PathVariable userId: String, @RequestParam(value = "cardpackId", required = true) cardpackId: String): ResponseEntity<Void> {
        try {
            database.getUser(userId)
            database.getCardpack(cardpackId)
        } catch (e: Exception) {
            return ResponseEntity.notFound().build()
        }

        return try {
            database.unfavoriteCardpack(userId, cardpackId)
            ResponseEntity.ok().build()
        } catch (e: Exception) {
            ResponseEntity.badRequest().build()
        }
    }

    @RequestMapping(value = "/user/{userId}/cardpacks/favorite", method = [RequestMethod.GET])
    @ApiOperation(value = "Retrieve favorited cardpacks")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved favorited cardpacks"),
            ApiResponse(code = 404, message = "User does not exist")
    )
    fun getFavoritedCardpacks(@PathVariable userId: String): ResponseEntity<List<CardpackModel>> {
        try {
            database.getUser(userId)
        } catch (e: Exception) {
            return ResponseEntity.notFound().build()
        }

        return ResponseEntity.ok(database.getFavoritedCardpacks(userId))
    }

    @RequestMapping(value = "/user/{userId}/cardpacks/favorite/{cardpackId}", method = [RequestMethod.GET])
    @ApiOperation(value = "Check whether a particular user has favorited a specific cardpack")
    @ApiResponses(
            ApiResponse(code = 200, message = "Successfully retrieved information"),
            ApiResponse(code = 404, message = "User/cardpack does not exist")
    )
    fun checkIfCardpackIsFavorited(@PathVariable userId: String, @PathVariable cardpackId: String): ResponseEntity<Boolean> {
        return try {
            database.getUser(userId)
            ResponseEntity.ok(database.cardpackIsFavoritedByUser(userId, cardpackId))
        } catch (e: Exception) {
            ResponseEntity.notFound().build()
        }
    }
}