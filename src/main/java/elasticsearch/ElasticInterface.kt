package elasticsearch

import route.card.model.CardpackModel
import route.user.model.UserModel

interface ElasticIndexer {
    fun indexUser(user: UserModel)
    fun indexUsers(users: List<UserModel>)
    fun indexCardpack(cardpack: CardpackModel)
    fun indexCardpacks(cardpacks: List<CardpackModel>)
    fun unindexCardpack(cardpackId: String)
    fun unindexCardpacks(cardpackIds: List<String>)
    fun reindexAllData()
}

interface ElasticSearcher {
    fun searchUsers(query: String): List<UserModel>
    fun searchCardpacks(query: String): List<CardpackModel>
}

interface ElasticAutoCompleter {
    fun autoCompleteUserSearch(query: String, max: Int = 10): List<String>
    fun autoCompleteCardpackSearch(query: String, max: Int = 10): List<String>
}

interface ElasticInterface : ElasticIndexer, ElasticSearcher, ElasticAutoCompleter