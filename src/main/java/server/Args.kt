package server

open class Args {
    val mongoHost: String = System.getenv("MONGO_HOST") ?: "localhost"
    val mongoPort: Int = try { System.getenv("MONGO_PORT").toInt() } catch (e: Exception) { 27017 }
    val mongoDatabase: String = System.getenv("MONGO_DATABASE") ?: "cardsOnline"
    val mongoUsername: String? = System.getenv("MONGO_USERNAME")
    val mongoPassword: String? = System.getenv("MONGO_PASSWORD")
    val elasticsearchHost: String = System.getenv("ELASTICSEARCH_HOST") ?: "localhost"
    val elasticsearchPort: Int = try { System.getenv("ELASTICSEARCH_PORT").toInt() } catch (e: Exception) { 9200 }
    val elasticsearchUsername: String? = System.getenv("ELASTICSEARCH_USERNAME")
    val elasticsearchPassword: String? = System.getenv("ELASTICSEARCH_PASSWORD")
    val elasticsearchConnectionScheme: String = System.getenv("ELASTICSEARCH_CONNECTION_SCHEME") ?: "http"
    val allowedCorsOrigin: String = System.getenv("ALLOWED_CORS_ORIGIN") ?: "http://localhost"
    val s3AccessKey: String = System.getenv("S3_ACCESS_KEY") ?: throw Exception("Missing env var: S3_ACCESS_KEY")
    val s3PrivateKey: String = System.getenv("S3_PRIVATE_KEY") ?: throw Exception("Missing env var: S3_PRIVATE_KEY")
    val s3Bucket: String = System.getenv("S3_BUCKET") ?: throw Exception("Missing env var: S3_BUCKET")
    val s3Endpoint: String = System.getenv("S3_ENDPOINT") ?: throw Exception("Missing env var: S3_ENDPOINT")
    val s3Region: String = System.getenv("S3_REGION") ?: throw Exception("Missing env var: S3_REGION")
}