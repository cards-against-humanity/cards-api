package objectstore

import com.google.common.net.MediaType

class MemoryObjectCollection : ObjectCollection {
    private val objects: MutableMap<String, ObjectCollection.ObjectWrapper> = HashMap()

    override fun putObject(key: String, data: ByteArray, type: MediaType) {
        if (!ObjectCollection.isValidKey(key)) {
            throw IllegalArgumentException("Invalid object key")
        }
        objects[key] = ObjectCollection.ObjectWrapper(data, type)
    }

    override fun getObject(key: String): ObjectCollection.ObjectWrapper {
        if (!ObjectCollection.isValidKey(key)) {
            throw IllegalArgumentException("Invalid object key")
        }
        return objects[key] ?: throw ObjectNotFoundException("Object does not exist with key: $key")
    }

    override fun deleteObject(key: String) {
        if (!ObjectCollection.isValidKey(key)) {
            throw IllegalArgumentException("Invalid object key")
        }
        if (!objects.containsKey(key)) {
            throw ObjectNotFoundException("Object does not exist with key: $key")
        }
        objects.remove(key)
    }
}