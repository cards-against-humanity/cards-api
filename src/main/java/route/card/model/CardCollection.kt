package route.card.model

import route.card.JsonBlackCard
import route.card.JsonWhiteCard

interface CardCollection {
    fun createCardpack(name: String, userId: String): CardpackModel
    fun deleteCardpack(id: String)
    fun getCardpack(id: String): CardpackModel
    fun getCardpacks(userId: String): List<CardpackModel>
    fun createWhiteCard(card: JsonWhiteCard, cardpackId: String): WhiteCardModel
    fun createWhiteCards(cards: List<JsonWhiteCard>, cardpackId: String): List<WhiteCardModel>
    fun createBlackCard(card: JsonBlackCard, cardpackId: String): BlackCardModel
    fun createBlackCards(cards: List<JsonBlackCard>, cardpackId: String): List<BlackCardModel>
    fun deleteWhiteCard(cardId: String, cardpackId: String)
    fun deleteWhiteCards(cardIds: List<String>, cardpackId: String)
    fun deleteBlackCard(cardId: String, cardpackId: String)
    fun deleteBlackCards(cardIds: List<String>, cardpackId: String)
    fun favoriteCardpack(userId: String, cardpackId: String)
    fun unfavoriteCardpack(userId: String, cardpackId: String)
    fun getFavoritedCardpacks(userId: String): List<CardpackModel>
    fun cardpackIsFavoritedByUser(userId: String, cardpackId: String): Boolean
    fun forEachCardpack(fn: (CardpackModel) -> Unit)
    fun filterUnusedCardpackIds(ids: List<String>): List<String>
}