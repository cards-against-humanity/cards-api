package objectstore

import com.google.common.net.MediaType
import java.util.Arrays

interface ObjectCollection {
    fun putObject(key: String, data: ByteArray, type: MediaType = MediaType.OCTET_STREAM)
    fun getObject(key: String): ObjectWrapper
    fun deleteObject(key: String)

    data class ObjectWrapper(
        val data: ByteArray,
        val type: MediaType
    ) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as ObjectWrapper

            if (!Arrays.equals(data, other.data)) return false
            if (type != other.type) return false

            return true
        }

        override fun hashCode(): Int {
            var result = Arrays.hashCode(data)
            result = 31 * result + type.hashCode()
            return result
        }
    }

    companion object {
        private val allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!-_.*'()/".toSet()

        fun isValidKey(key: String): Boolean {
            if (key.isEmpty() || containsAdjacentSlashes(key) || key[0] == '/' || key[key.length - 1] == '/') {
                return false
            }

            key.forEach {
                if (!allowedChars.contains(it)) {
                    return false
                }
            }

            return true
        }

        private fun containsAdjacentSlashes(s: String): Boolean {
            var lastSlashIndex: Int = -2
            s.forEachIndexed { i, char ->
                if (char == '/') {
                    if (lastSlashIndex == i - 1) {
                        return true
                    }
                    lastSlashIndex = i
                }
            }
            return false
        }
    }
}