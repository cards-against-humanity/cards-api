package elasticsearch

import database.DatabaseCollection
import route.card.model.CardpackModel
import route.user.model.UserModel

class ElasticSearchableDatabaseCollection(private val superCollection: DatabaseCollection, private val elasticClient: ElasticInterface) : DatabaseCollection by superCollection, SearchableDatabaseCollection {

    // TODO - Update name of users and cardpacks when they are changed

    override fun searchUsers(query: String): List<UserModel> {
        return elasticClient.searchUsers(query)
    }

    override fun searchCardpacks(query: String): List<CardpackModel> {
        return elasticClient.searchCardpacks(query)
    }

    override fun createUser(name: String, oAuthId: String, oAuthProvider: String): UserModel {
        val userModel = superCollection.createUser(name, oAuthId, oAuthProvider)
        elasticClient.indexUser(userModel)
        return userModel
    }

    override fun setUserName(id: String, name: String): UserModel {
        val userModel = superCollection.setUserName(id, name)
        elasticClient.indexUser(userModel)
        return userModel
    }

    override fun createCardpack(name: String, userId: String): CardpackModel {
        val cardpackModel = superCollection.createCardpack(name, userId)
        elasticClient.indexCardpack(cardpackModel)
        return cardpackModel
    }

    override fun deleteCardpack(id: String) {
        elasticClient.unindexCardpack(id)
        superCollection.deleteCardpack(id)
    }

    override fun autoCompleteUserSearch(query: String, max: Int): List<String> {
        return elasticClient.autoCompleteUserSearch(query, max)
    }

    override fun autoCompleteCardpackSearch(query: String, max: Int): List<String> {
        return elasticClient.autoCompleteCardpackSearch(query, max)
    }
}