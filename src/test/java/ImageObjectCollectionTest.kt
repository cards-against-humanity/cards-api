import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.AnonymousAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.google.common.net.MediaType
import io.findify.s3mock.S3Mock
import objectstore.ImageObjectCollection
import objectstore.MemoryObjectCollection
import objectstore.S3ObjectCollection
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import java.io.File
import java.nio.charset.Charset
import kotlin.test.assertEquals

class ImageObjectCollectionTest {
    companion object {
        private val s3Mock = S3Mock.Builder().withPort(8002).withInMemoryBackend().build()
        private val endpoint = AwsClientBuilder.EndpointConfiguration("http://localhost:8002", "us-west-2")
        private var imageCollections = listOf<ImageObjectCollection>()

        init {
            s3Mock.start()
        }

        @AfterAll
        fun teardown() {
            s3Mock.shutdown()
        }
    }

    @BeforeEach
    fun reset() {
        val s3Client = AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpoint)
                .withCredentials(AWSStaticCredentialsProvider(AnonymousAWSCredentials()))
                .build()
        if (s3Client.doesBucketExistV2("testbucket")) {
            s3Client.deleteBucket("testbucket")
        }
        s3Client.createBucket("testbucket")
        imageCollections = listOf(ImageObjectCollection(MemoryObjectCollection()), ImageObjectCollection(S3ObjectCollection(s3Client, "testbucket")))
    }

    @TestFactory
    fun storeAndRetrievePNG(): List<DynamicTest> {
        return imageCollections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            val imageBytes = File("src/test/resources/test.png").readBytes()
            collection.putImage("test", imageBytes)
            val imageObj = collection.getImage("test")
            assertEquals(MediaType.PNG, imageObj.type)
            assertEquals(String(imageBytes, Charset.defaultCharset()), String(imageObj.data, Charset.defaultCharset()))
        } }
    }

    @TestFactory
    fun storeAndRetrieveJPEG(): List<DynamicTest> {
        return imageCollections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            val imageBytes = File("src/test/resources/test.jpeg").readBytes()
            collection.putImage("test", imageBytes)
            val imageObj = collection.getImage("test")
            assertEquals(MediaType.JPEG, imageObj.type)
            assertEquals(String(imageBytes, Charset.defaultCharset()), String(imageObj.data, Charset.defaultCharset()))
        } }
    }

    @TestFactory
    fun cannotStoreGIF(): List<DynamicTest> {
        return imageCollections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            val imageBytes = File("src/test/resources/test.gif").readBytes()
            val e = assertThrows(IllegalArgumentException::class.java) { collection.putImage("test", imageBytes) }
            assertEquals("Image must be web-safe (png or jpeg)", e.message)
        } }
    }

//    TODO - Enable this test after upgrading to JDK 10
//    @TestFactory
//    fun cannotStoreTIFF(): List<DynamicTest> {
//        return imageCollections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
//            val imageBytes = File("src/test/resources/test.tiff").readBytes()
//            val e = assertThrows(IllegalArgumentException::class.java) { collection.putImage("test", imageBytes) }
//            assertEquals("Image must be web-safe (png or jpeg)", e.message)
//        } }
//    }

    @TestFactory
    fun cannotStoreBMP(): List<DynamicTest> {
        return imageCollections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            val imageBytes = File("src/test/resources/test.bmp").readBytes()
            val e = assertThrows(IllegalArgumentException::class.java) { collection.putImage("test", imageBytes) }
            assertEquals("Image must be web-safe (png or jpeg)", e.message)
        } }
    }

    @TestFactory
    fun cannotStoreArbitraryData(): List<DynamicTest> {
        return imageCollections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            val e = assertThrows(Exception::class.java) { collection.putImage("test", "Hello world".toByteArray()) }
            assertEquals("Data must be an image", e.message)
        } }
    }
}