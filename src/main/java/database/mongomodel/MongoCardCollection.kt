package database.mongomodel

import com.mongodb.client.MongoCollection
import org.bson.Document
import org.bson.types.ObjectId
import route.card.JsonBlackCard
import route.card.JsonWhiteCard
import route.card.model.BlackCardModel
import route.card.model.CardCollection
import route.card.model.CardpackModel
import route.card.model.WhiteCardModel
import route.user.model.UserCollection
import route.user.model.UserModel
import java.util.Date

class MongoCardCollection(private val mongoCollectionCardpacks: MongoCollection<Document>, private val userCollection: UserCollection) : CardCollection {
    override fun createCardpack(name: String, userId: String): CardpackModel {
        val user = userCollection.getUser(userId)
        val id = ObjectId()
        mongoCollectionCardpacks.insertOne(Document()
                .append("_id", id)
                .append("name", name)
                .append("ownerId", userId)
                .append("subscribers", listOf<String>())
                .append("whiteCards", listOf<Any>())
                .append("blackCards", listOf<Any>())
        )
        return MongoCardpackModel(id.toHexString(), name, user, ArrayList(), ArrayList(), id.date, 0L, mongoCollectionCardpacks)
    }

    override fun deleteCardpack(id: String) {
        val mongoId = try {
            ObjectId(id)
        } catch (e: IllegalArgumentException) {
            throw Exception("Cardpack does not exist with id: $id")
        }
        // TODO - Make these writes atomic
        val res = mongoCollectionCardpacks.deleteOne(Document("_id", mongoId))
        if (res.wasAcknowledged() && res.deletedCount == 0L) {
            throw Exception("Cardpack does not exist with id: $id")
        }
    }

    override fun getCardpack(id: String): CardpackModel {
        try {
            return findCardpacks(Document("_id", ObjectId(id)), onlyFirst = true)[0]
        } catch (e: Exception) {
            throw Exception("Cardpack does not exist with id: $id")
        }
    }

    override fun getCardpacks(userId: String): List<CardpackModel> {
        userCollection.getUser(userId)
        return findCardpacks(Document("ownerId", userId), sameUser = true)
    }

    override fun createWhiteCard(card: JsonWhiteCard, cardpackId: String): WhiteCardModel {
        return createWhiteCards(listOf(card), cardpackId)[0]
    }

    override fun createWhiteCards(cards: List<JsonWhiteCard>, cardpackId: String): List<WhiteCardModel> {
        val cardIds = cards.map { ObjectId() }
        val mongoCards = cards.mapIndexed { i, card ->
            Document()
                    .append("_id", cardIds[i])
                    .append("text", card.text)
                    .append("cardpackId", cardpackId)
        }
        val cardpackObjectId = try {
            ObjectId(cardpackId)
        } catch (e: Exception) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }

        val res = mongoCollectionCardpacks.updateOne(
                Document("_id", cardpackObjectId),
                Document("\$push", Document("whiteCards", Document("\$each", mongoCards)))
        )
        if (res.modifiedCount == 0L) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        return cards.mapIndexed { i, card ->
            MongoWhiteCardModel(cardIds[i].toHexString(), card.text, cardpackId, mongoCollectionCardpacks)
        }
    }

    override fun createBlackCard(card: JsonBlackCard, cardpackId: String): BlackCardModel {
        return createBlackCards(listOf(card), cardpackId)[0]
    }

    override fun createBlackCards(cards: List<JsonBlackCard>, cardpackId: String): List<BlackCardModel> {
        val cardIds = cards.map { ObjectId() }
        val mongoCards = cards.mapIndexed { i, card ->
            Document()
                    .append("_id", cardIds[i])
                    .append("text", card.text)
                    .append("answerFields", card.answerFields)
                    .append("cardpackId", cardpackId)
        }
        val cardpackObjectId = try {
            ObjectId(cardpackId)
        } catch (e: Exception) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }

        val res = mongoCollectionCardpacks.updateOne(
                Document("_id", cardpackObjectId),
                Document("\$push", Document("blackCards", Document("\$each", mongoCards)))
        )
        if (res.modifiedCount == 0L) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        return cards.mapIndexed { i, card ->
            MongoBlackCardModel(cardIds[i].toHexString(), card.text, card.answerFields, cardpackId, mongoCollectionCardpacks)
        }
    }

    override fun deleteWhiteCard(cardId: String, cardpackId: String) {
        val cardpackObjectId = try {
            ObjectId(cardpackId)
        } catch (e: Exception) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        val cardObjectId = try {
            ObjectId(cardId)
        } catch (e: Exception) {
            throw Exception("Card does not exist with id: $cardId")
        }
        val res = mongoCollectionCardpacks.updateOne(
                Document("_id", cardpackObjectId),
                Document("\$pull", Document("whiteCards", Document("_id", cardObjectId)))
        )
        if (res.matchedCount == 0L) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        if (res.modifiedCount == 0L) {
            throw Exception("Card does not exist with id: $cardId")
        }
    }

    override fun deleteWhiteCards(cardIds: List<String>, cardpackId: String) {
        // TODO - Assert atomicity
        val cardpackObjectId = try {
            ObjectId(cardpackId)
        } catch (e: Exception) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        val cardObjectIds = try {
            cardIds.map { ObjectId(it) }
        } catch (e: Exception) {
            throw Exception("One or more card ids is invalid")
        }
        // TODO - Make this method atomic (currently performs find() and updateOne())
        if (mongoCollectionCardpacks.find(Document("_id", cardpackObjectId)).first() == null) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        val res = mongoCollectionCardpacks.updateOne(
                Document("_id", cardpackObjectId).append("whiteCards", Document("\$all", cardObjectIds.map { Document("\$elemMatch", Document("_id", it)) })),
                Document("\$pull", Document("whiteCards", Document("_id", Document("\$in", cardObjectIds))))
        )
        if (res.modifiedCount == 0L) {
            throw Exception("One or more card ids is invalid")
        }
    }

    override fun deleteBlackCard(cardId: String, cardpackId: String) {
        val cardpackObjectId = try {
            ObjectId(cardpackId)
        } catch (e: Exception) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        val cardObjectId = try {
            ObjectId(cardId)
        } catch (e: Exception) {
            throw Exception("Card does not exist with id: $cardId")
        }
        val res = mongoCollectionCardpacks.updateOne(
                Document("_id", cardpackObjectId),
                Document("\$pull", Document("blackCards", Document("_id", cardObjectId)))
        )
        if (res.matchedCount == 0L) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        if (res.modifiedCount == 0L) {
            throw Exception("Card does not exist with id: $cardId")
        }
    }

    override fun deleteBlackCards(cardIds: List<String>, cardpackId: String) {
        // TODO - Assert atomicity
        val cardpackObjectId = try {
            ObjectId(cardpackId)
        } catch (e: Exception) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        val cardObjectIds = try {
            cardIds.map { ObjectId(it) }
        } catch (e: Exception) {
            throw Exception("One or more card ids is invalid")
        }
        // TODO - Make this method atomic (currently performs find() and updateOne())
        if (mongoCollectionCardpacks.find(Document("_id", cardpackObjectId)).first() == null) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        val res = mongoCollectionCardpacks.updateOne(
                Document("_id", cardpackObjectId).append("blackCards", Document("\$all", cardObjectIds.map { Document("\$elemMatch", Document("_id", it)) })),
                Document("\$pull", Document("blackCards", Document("_id", Document("\$in", cardObjectIds))))
        )
        if (res.modifiedCount == 0L) {
            throw Exception("One or more card ids is invalid")
        }
    }

    override fun favoriteCardpack(userId: String, cardpackId: String) {
        if (userId == getCardpack(cardpackId).owner.id) {
            throw Exception("Cannot favorite your own cardpack")
        }
        userCollection.getUser(userId)
        val res = mongoCollectionCardpacks.updateOne(Document("_id", ObjectId(cardpackId)), Document("\$addToSet", Document("subscribers", userId)))
        if (res.wasAcknowledged() && res.modifiedCount == 0L) {
            throw Exception("Cardpack is already in your favorites")
        }
    }

    override fun unfavoriteCardpack(userId: String, cardpackId: String) {
        val res = mongoCollectionCardpacks.updateOne(Document("_id", ObjectId(cardpackId)), Document("\$pull", Document("subscribers", userId)))
        if (res.wasAcknowledged() && res.modifiedCount == 0L) {
            throw Exception("Cardpack is not in your favorites")
        }
    }

    override fun getFavoritedCardpacks(userId: String): List<CardpackModel> {
        return findCardpacks(Document("subscribers", userId))
    }

    override fun cardpackIsFavoritedByUser(userId: String, cardpackId: String): Boolean {
        val cardpackDoc = mongoCollectionCardpacks.find(Document("_id", ObjectId(cardpackId))).first() ?: throw Exception("Cardpack does not exist with id: $cardpackId")
        val subscribers = cardpackDoc["subscribers"] as List<*>
        return subscribers.contains(userId)
    }

    override fun forEachCardpack(fn: (CardpackModel) -> Unit) {
        mongoCollectionCardpacks.find().forEach { fn(convertDocumentToMongoCardpackModel(it)) }
    }

    override fun filterUnusedCardpackIds(ids: List<String>): List<String> {
        // Add all IDs to begin, then remove them if they exist in the database
        val unusedCardpackIds: MutableSet<String> = ids.toMutableSet()
        val objectIds: MutableList<ObjectId> = ArrayList()
        ids.forEach {
            if (ObjectId.isValid(it)) {
                objectIds.add(ObjectId(it))
            }
        }
        mongoCollectionCardpacks.find(Document("_id", Document("\$in", objectIds))).forEach {
            val id = it["_id"] as ObjectId
            unusedCardpackIds.remove(id.toHexString())
        }
        return unusedCardpackIds.toList()
    }

    private fun convertDocumentToMongoCardpackModel(doc: Document): MongoCardpackModel {
        val id = (doc["_id"] as ObjectId).toHexString()

        return MongoCardpackModel(
                id,
                doc["name"] as String,
                userCollection.getUser(doc["ownerId"] as String),
                (doc["whiteCards"] as List<Document>).map { MongoWhiteCardModel(it, mongoCollectionCardpacks) },
                (doc["blackCards"] as List<Document>).map { MongoBlackCardModel(it, mongoCollectionCardpacks) },
                (doc["_id"] as ObjectId).date,
                (doc["subscribers"] as List<*>).size.toLong(),
                mongoCollectionCardpacks
        )
    }

    private fun findCardpacks(findDoc: Document, sameUser: Boolean = false, onlyFirst: Boolean = false): List<CardpackModel> {
        val docsCursor = mongoCollectionCardpacks.find(findDoc)

        val cardpackDocs = if (onlyFirst) {
            listOf(docsCursor.first())
        } else {
            docsCursor.toList()
        }

        if (cardpackDocs.isEmpty()) {
            return listOf()
        }

        val sameUser = if (sameUser) {
            userCollection.getUser(cardpackDocs[0]["ownerId"] as String)
        } else {
            null
        }

        return cardpackDocs.map {
            val id = (it["_id"] as ObjectId).toHexString()

            MongoCardpackModel(
                    id,
                    it["name"] as String,
                    sameUser ?: userCollection.getUser(it["ownerId"] as String),
                    (it["whiteCards"] as List<Document>).map { MongoWhiteCardModel(it, mongoCollectionCardpacks) },
                    (it["blackCards"] as List<Document>).map { MongoBlackCardModel(it, mongoCollectionCardpacks) },
                    (it["_id"] as ObjectId).date,
                    (it["subscribers"] as List<*>).size.toLong(),
                    mongoCollectionCardpacks
            )
        }
    }

    private class MongoCardpackModel(
        override val id: String,
        override var name: String,
        override val owner: UserModel,
        override val whiteCards: List<WhiteCardModel>,
        override val blackCards: List<BlackCardModel>,
        override val createdAt: Date,
        override val likeCount: Long,
        private val mongoCollectionCardpacks: MongoCollection<Document>
    ) : CardpackModel {

        override fun setName(name: String): CardpackModel {
            mongoCollectionCardpacks.updateOne(Document("_id", ObjectId(this.id)), Document("\$set", Document("name", name)))
            this.name = name
            return this
        }
    }

    private class MongoWhiteCardModel(override val id: String, override var text: String, override val cardpackId: String, private val mongoCollectionCardpacks: MongoCollection<Document>) : WhiteCardModel {
        constructor(json: Document, mongoCollectionCardpacks: MongoCollection<Document>) : this((json["_id"] as ObjectId).toHexString(), json["text"] as String, json["cardpackId"] as String, mongoCollectionCardpacks)

        override fun setText(text: String): WhiteCardModel {
            mongoCollectionCardpacks.updateOne(Document("_id", ObjectId(this.cardpackId)).append("whiteCards._id", ObjectId(id)), Document("\$set", Document("whiteCards.$.text", text)))
            this.text = text
            return this
        }
    }

    private class MongoBlackCardModel(override val id: String, override var text: String, override val answerFields: Int, override val cardpackId: String, private val mongoCollectionCardpacks: MongoCollection<Document>) : BlackCardModel {
        constructor(json: Document, mongoCollectionCardpacks: MongoCollection<Document>) : this((json["_id"] as ObjectId).toHexString(), json["text"] as String, json["answerFields"] as Int, json["cardpackId"] as String, mongoCollectionCardpacks)

        override fun setText(text: String): BlackCardModel {
            mongoCollectionCardpacks.updateOne(Document("_id", ObjectId(this.cardpackId)).append("blackCards._id", ObjectId(id)), Document("\$set", Document("blackCards.$.text", text)))
            this.text = text
            return this
        }
    }
}