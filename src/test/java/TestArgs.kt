open class TestArgs {
    val mongoHost: String = System.getenv("MONGO_HOST") ?: "localhost"
    val mongoPort: Int = try { System.getenv("MONGO_PORT").toInt() } catch (e: Exception) { 27017 }
    val mongoDatabase: String = System.getenv("MONGO_DATABASE") ?: "cardsApiTest"
    val elasticsearchHost: String = System.getenv("ELASTICSEARCH_HOST") ?: "localhost"
    val elasticsearchPort: Int = try { System.getenv("ELASTICSEARCH_PORT").toInt() } catch (e: Exception) { 9200 }
}

// TODO - Document this in readme file