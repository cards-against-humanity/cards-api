package objectstore

import com.google.common.net.MediaType
import java.net.URLConnection

class ImageObjectCollection(private val objectCollection: ObjectCollection) {
    fun putImage(key: String, data: ByteArray) {
        val type = getImageType(data)
        if (type == null || type.type() != "image") {
            throw IllegalArgumentException("Data must be an image")
        }
        if (type.subtype() != "png" && type.subtype() != "jpeg") {
            throw IllegalArgumentException("Image must be web-safe (png or jpeg)")
        }
        objectCollection.putObject(key, data, type)
    }

    fun getImage(key: String): ObjectCollection.ObjectWrapper {
        val obj = objectCollection.getObject(key)
        val type = getImageType(obj.data)
        if (type == null || type.type() != "image") {
            throw IllegalArgumentException("Data must be an image")
        }
        if (type.subtype() != "png" && type.subtype() != "jpeg") {
            throw IllegalArgumentException("Image must be web-safe (png or jpeg)")
        }
        return obj
    }

    fun deleteImage(key: String) {
        objectCollection.deleteObject(key)
    }

    companion object {
        private fun getImageType(data: ByteArray): MediaType? {
            if (String(data.copyOfRange(0, 2)) == "BM") {
                return MediaType.BMP
            }
            return MediaType.parse(URLConnection.guessContentTypeFromStream(data.inputStream()) ?: return null)
        }
    }
}