package objectstore

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.ObjectMetadata
import com.google.common.net.MediaType

class S3ObjectCollection(private val s3Client: AmazonS3, private val bucketName: String) : ObjectCollection {
    constructor(bucketName: String, accessKey: String, secretKey: String, serviceEndpoint: String, serviceRegion: String) : this(
            AmazonS3Client.builder()
                    .withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(serviceEndpoint, serviceRegion))
                    .withCredentials(
                            AWSStaticCredentialsProvider(
                                    BasicAWSCredentials(accessKey, secretKey)
                            )
                    )
                    .build(),
            bucketName
    )

    override fun putObject(key: String, data: ByteArray, type: MediaType) {
        if (!ObjectCollection.isValidKey(key)) {
            throw IllegalArgumentException("Invalid object key")
        }
        val metadata = ObjectMetadata()
        metadata.contentType = type.toString()
        s3Client.putObject(bucketName, key, data.inputStream(), metadata)
    }

    override fun getObject(key: String): ObjectCollection.ObjectWrapper {
        if (!ObjectCollection.isValidKey(key)) {
            throw IllegalArgumentException("Invalid object key")
        }
        val res = try {
            s3Client.getObject(bucketName, key)
        } catch (e: AmazonS3Exception) {
            throw if (e.errorCode == "NoSuchKey") {
                ObjectNotFoundException("Object does not exist with key: $key")
            } else {
                e
            }
        }
        return ObjectCollection.ObjectWrapper(res.objectContent.readBytes(), MediaType.parse(res.objectMetadata.contentType))
    }

    override fun deleteObject(key: String) {
        if (!ObjectCollection.isValidKey(key)) {
            throw IllegalArgumentException("Invalid object key")
        }
        try {
            s3Client.deleteObject(bucketName, key)
        } catch (e: AmazonS3Exception) {
            throw if (e.errorCode == "404 Not Found") {
                ObjectNotFoundException("Object does not exist with key: $key")
            } else {
                e
            }
        }
    }
}